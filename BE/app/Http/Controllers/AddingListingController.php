<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\playlist_songs;

class AddingListingController extends Controller
{
    public function AddingListing(){
        return DB::table('playlist_songs')->get();
    }

    //Upload File
    public function UploadingSongs(Request $request){

        $AL = new Playlist_Songs();
        $AL->song_id = $request->song_id;
        $AL->playlist_id = $request->playlist_id;
        $AL->save();
        return $AL;
    }

}
