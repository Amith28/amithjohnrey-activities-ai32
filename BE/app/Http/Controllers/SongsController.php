<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\songs;

class SongsController extends Controller
{
    public function displayingSongs(){
        return DB::table('songs')->get();
    }

    //Upload File
    public function UploadingSongs(Request $request){

        $NewS = new Songs();
        $NewS->title = $request->title;
        $NewS->length = $request->length;
        $NewS->artist = $request->artist;
        $NewS->save();
        return $NewS;
    }
}