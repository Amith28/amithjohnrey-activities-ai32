<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\SongsController;
use App\Http\Controllers\PlaylistsController;
use App\Http\Controllers\AddingListingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Songs route
Route::get('/songs', [SongsController::class, 'displayingSongs']);
Route::post('/upload', [SongsController::class, 'UploadingSongs']);
//Playlist route
Route::get('/playlist', [PlaylistsController::class, 'displayingPlaylist']);
Route::post('/create', [PlaylistsController::class, 'UploadingSongs']);
//PlaylistSong route
Route::get('/playlistsongs', [AddingListingController::class, 'AddingListing']);
Route::post('/create', [AddingListingController::class, 'UploadingSongs']);